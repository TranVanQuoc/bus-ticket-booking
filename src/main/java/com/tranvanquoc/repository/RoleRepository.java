package com.tranvanquoc.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tranvanquoc.entity.Role;
@Repository
public interface RoleRepository extends JpaRepository<Role, Integer>{
	
}
