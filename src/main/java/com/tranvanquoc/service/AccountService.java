package com.tranvanquoc.service;

import com.tranvanquoc.entity.User;
import com.tranvanquoc.model.LoginModel;

public interface AccountService {
	LoginModel findUserByUserName(String username);
	User login(String username);
	User register(User user);
	boolean checkEmail(String email);
	boolean checkUsername(String username);
	String getName(String username);
	User getUserByUsername(String username);
	boolean updateProfile(User user);
}
