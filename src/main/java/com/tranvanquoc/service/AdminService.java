package com.tranvanquoc.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.tranvanquoc.entity.User;
import com.tranvanquoc.model.AdminChart;

public interface AdminService {
	List<User> findAll();

	User updateUser(User user);

	AdminChart getChart();

	Page<User> getUserPage(int role, String keySearch, int creator, Pageable pageable);
}
