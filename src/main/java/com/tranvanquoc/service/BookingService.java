package com.tranvanquoc.service;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.tranvanquoc.entity.Booking;
import com.tranvanquoc.entity.User;
import com.tranvanquoc.model.Message;

public interface BookingService {
	List<Booking> getBookingByDT(int trip_id, Date date);
	Page<Booking> getListBookingByUser(int user_id, String start_date, String end_date, Pageable pageable);
	boolean updateStatus(int id, int status);
	Message checkBeforeBooking(int trip_id, Date date, String[] lstSeat, int bus_id);
	int saveBookingList(int trip_id, int user_id, String[] lstSeat, String date, String cardNumber) throws ParseException;
	Booking getOne(int id);
}
