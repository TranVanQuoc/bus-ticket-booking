package com.tranvanquoc.service;

import java.util.List;

import com.tranvanquoc.entity.Role;

public interface RoleService {
	List<Role> findAllRole();
}
