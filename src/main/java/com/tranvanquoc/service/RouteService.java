package com.tranvanquoc.service;

import java.util.List;

import com.tranvanquoc.entity.Route;

public interface RouteService{
	List<Route> findAll();
	List<Route> findDistinct();
	List<Route> findDest(int id);
}
