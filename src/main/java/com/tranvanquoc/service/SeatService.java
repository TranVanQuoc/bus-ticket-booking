package com.tranvanquoc.service;

import java.util.List;

import com.tranvanquoc.entity.Seat;

public interface SeatService {
	List<Seat> getSeatNotAvailableByBusId(int bus_id);
}
