package com.tranvanquoc.service;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.tranvanquoc.entity.Booking;
import com.tranvanquoc.entity.Bus;
import com.tranvanquoc.entity.Seat;

public interface StaffService {
	Page<Bus> getBus(String cardNumber, Pageable pageable);
	List<Booking> getTicket();
	List<Seat> getAllSeatOfBus(String cardNumber);
	boolean updateStatusSeat(int seatId, int status);
	Page<Booking> getBooking(String search, Date start_date, Date end_date, Pageable pageable);
}
