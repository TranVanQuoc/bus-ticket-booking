package com.tranvanquoc.service;

import java.util.List;

import com.tranvanquoc.entity.Trip;

public interface TripService {
	List<Trip> findTripByRouteId(int route_id);
	List<Trip> findAll();
}
