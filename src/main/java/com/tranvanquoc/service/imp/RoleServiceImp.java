package com.tranvanquoc.service.imp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tranvanquoc.entity.Role;
import com.tranvanquoc.repository.RoleRepository;
import com.tranvanquoc.service.RoleService;

@Service
public class RoleServiceImp implements RoleService{

	@Autowired
	private RoleRepository roleRepository;
	
	@Override
	public List<Role> findAllRole() {
		return roleRepository.findAll();
	}

}
