package com.tranvanquoc.service.imp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tranvanquoc.entity.Route;
import com.tranvanquoc.repository.RouteRepository;
import com.tranvanquoc.service.RouteService;

@Service
public class RouteServiceImp implements RouteService{
	@Autowired
	RouteRepository routeRepository;
	
	public List<Route> findAll(){
		return routeRepository.findAll();
	}

	@Override
	public List<Route> findDistinct() {
		return routeRepository.findDistinct();
	}

	@Override
	public List<Route> findDest(int src) {
		return routeRepository.findDest(src);
	}
}
