package com.tranvanquoc.service.imp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tranvanquoc.entity.Seat;
import com.tranvanquoc.repository.SeatRepository;
import com.tranvanquoc.service.SeatService;

@Service
public class SeatServiceImp implements SeatService {
	@Autowired
	private SeatRepository seatRepository;
	
	@Override
	public List<Seat> getSeatNotAvailableByBusId(int bus_id) {
		return seatRepository.getSeatNotAvailableByBusId(bus_id);
	}
	
}
