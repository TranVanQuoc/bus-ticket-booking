package com.tranvanquoc.service.imp;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.tranvanquoc.entity.Booking;
import com.tranvanquoc.entity.Bus;
import com.tranvanquoc.entity.Seat;
import com.tranvanquoc.repository.BookingRepository;
import com.tranvanquoc.repository.BusRepository;
import com.tranvanquoc.repository.SeatRepository;
import com.tranvanquoc.service.StaffService;

@Service
public class StaffServiceImp implements StaffService {

	@Autowired
	private BusRepository busRepository;
	@Autowired
	private BookingRepository bookingRepository;
	@Autowired
	private SeatRepository seatRepository;

	@Override
	public Page<Bus> getBus(String cardNumber, Pageable pageable) {
		return busRepository.getBusByCardNumberContaining(cardNumber, pageable);
	}

	@Override
	public List<Booking> getTicket() {
		return bookingRepository.findAll();
	}

	@Override
	public List<Seat> getAllSeatOfBus(String cardNumber) {
		return seatRepository.getAllSeatByBusId(cardNumber);
	}

	@Override
	public boolean updateStatusSeat(int seatId, int status) {
		Seat s = seatRepository.findById(seatId);
		s.setStatus(status);

		Seat s1 = seatRepository.saveAndFlush(s);
		return s1 != null ? true : false;
	}

	@Override
	public Page<Booking> getBooking(String search, Date start_date, Date end_date, Pageable pageable) {
		return bookingRepository.getListBooking(start_date, end_date, search, pageable);
	}

}
